package com.cloudconnectiv.mobilenoc.app;

import android.content.Context;
import android.util.Log;

import com.android.volley.VolleyError;
import com.cloudconnectiv.mobilenoc.features.servers.pojo.ServersListPojo;
import com.cloudconnectiv.mobilenoc.network.IServiceObjectCallBack;
import com.cloudconnectiv.mobilenoc.network.IVolleyResult;
import com.cloudconnectiv.mobilenoc.network.VolleyManager;
import com.cloudconnectiv.mobilenoc.utility.Constants;
import com.cloudconnectiv.mobilenoc.utility.GsonHelper;

/**
 * Created by MaysAtari on 8/29/2017.
 */

public class WebServicesController {

    private static final int SERVERS_LIST_TAG = 100;

    public static void getServersListWS(Context context, int pageNum, int pageSize, boolean showProgress,
                                        final IServiceObjectCallBack iServiceObjectCallBack) {

        String url = String.format(Constants.getServersListLink(), pageNum, pageSize);

        IVolleyResult mResultCallback = new IVolleyResult() {
            @Override
            public void notifySuccess(String response) {
                Log.d("Volley JSON response", response);

                try {
                    ServersListPojo tempModel = GsonHelper.getParsedObject(response,
                            ServersListPojo.class);

                    iServiceObjectCallBack.onWebServiceSuccess(tempModel);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void notifyError(VolleyError error) {
                Log.d("Volley JSON error", "That didn't work! " + error.getMessage());
                iServiceObjectCallBack.OnWebServiceError(error.getMessage());
            }
        };
        VolleyManager volleyManager = new VolleyManager(mResultCallback, context);
        volleyManager.makeJSONGetObjectRequest(context, url, SERVERS_LIST_TAG, showProgress);
    }

}

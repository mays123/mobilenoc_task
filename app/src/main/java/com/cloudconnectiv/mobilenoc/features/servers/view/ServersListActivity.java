package com.cloudconnectiv.mobilenoc.features.servers.view;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.annimon.stream.Collectors;
import com.annimon.stream.Stream;
import com.cloudconnectiv.mobilenoc.R;
import com.cloudconnectiv.mobilenoc.app.WebServicesController;
import com.cloudconnectiv.mobilenoc.features.servers.adapter.ServersListAdapter;
import com.cloudconnectiv.mobilenoc.features.servers.pojo.Content;
import com.cloudconnectiv.mobilenoc.features.servers.pojo.ServersListPojo;
import com.cloudconnectiv.mobilenoc.features.shared.BaseActivity;
import com.cloudconnectiv.mobilenoc.network.IServiceObjectCallBack;
import com.cloudconnectiv.mobilenoc.utility.Constants;
import com.cloudconnectiv.mobilenoc.utility.Util;

import java.util.ArrayList;
import java.util.List;

public class ServersListActivity extends BaseActivity implements IServiceObjectCallBack,
        ServersListAdapter.OnServerItemClickListener {

    private TextView mNoServersTxt;
    private RecyclerView mServersRecyclerView;
    private ServersListAdapter mServersAdapter;
    private List<Content> mDownServersList = new ArrayList<>();
    private List<Content> mOtherServersList = new ArrayList<>();
    private Context mContext = ServersListActivity.this;
    private int pageNum = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        try {
            initializeViews();
            setUpServersRecyclerView();
            getServersDataByPage(true);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setUpServersRecyclerView() {
        LinearLayoutManager mServersLayoutManager = new LinearLayoutManager(this,
                LinearLayoutManager.VERTICAL, false);
        mServersRecyclerView.setLayoutManager(mServersLayoutManager);
        mServersAdapter = new ServersListAdapter(mContext, mDownServersList, mOtherServersList,
                this, mServersRecyclerView);
        mServersRecyclerView.setAdapter(mServersAdapter);
        mServersAdapter.setOnLoadMoreListener(loadMoreListener);
    }

    private void initializeViews() {
        mServersRecyclerView = findViewById(R.id.rvServers);
        mNoServersTxt = findViewById(R.id.tvNoServers);
    }

    private void getServersDataByPage(boolean showProgress) {
        WebServicesController.getServersListWS(mContext, pageNum, Constants.PAGE_SIZE, showProgress,
                this);
    }

    @Override
    protected int getActivityLayout() {
        return R.layout.activity_servers_list;
    }

    @Override
    public <T> void onWebServiceSuccess(T returnedObject) {

        ServersListPojo serversListPojo = (ServersListPojo) returnedObject;

        if (serversListPojo == null)
            return;

        int totalPages = serversListPojo.getTotalPages();
        int totalItems = serversListPojo.getTotalElements();

        if (totalItems > 0) {
            mNoServersTxt.setVisibility(View.GONE);
            mServersRecyclerView.setVisibility(View.VISIBLE);
            setData(serversListPojo.getContent());
        } else {
            mNoServersTxt.setVisibility(View.VISIBLE);
            mServersRecyclerView.setVisibility(View.GONE);
        }

        if (pageNum >= totalPages - 1) {
            mServersAdapter.setLoaded();
            mServersAdapter.setOnLoadMoreListener(null);
        } else {
            pageNum++;
        }


    }

    private void setData(List<Content> list) {
        if ((mDownServersList.size() + mOtherServersList.size()) > 0) {
            mServersAdapter.removeOtherServer(mOtherServersList.size() - 1);
        }
        if (pageNum == 0) {
            filterFirstTimeData(list);
        } else {
            filterRestOfData(list);
        }

        mServersAdapter.setLoaded();
    }

    private void filterFirstTimeData(List<Content> list) {
        //down servers list
        List<Content> downList = filterByDownServers(list);

        //other servers list
        List<Content> otherList = filterByOtherServers(list);

        mServersAdapter.addServers(downList, otherList);
    }

    private List<Content> filterByDownServers(List<Content> list) {
        return Stream.of(list)
                .filter(p -> p.getStatus().getStatusValue().equalsIgnoreCase("INSTANCE_STATE_DOWN"))
                .collect(Collectors.toList());
    }

    private List<Content> filterByOtherServers(List<Content> list) {
        return Stream.of(list)
                .filter(p -> !p.getStatus().getStatusValue().equalsIgnoreCase("INSTANCE_STATE_DOWN"))
                .collect(Collectors.toList());
    }

    private void filterRestOfData(List<Content> list) {
        //down servers list
        List<Content> downList = filterByDownServers(list);

        //other servers list
        List<Content> otherList = filterByOtherServers(list);

        mServersAdapter.addRangeServers(downList, otherList,
                (mDownServersList.size() + mOtherServersList.size()), list.size());
    }


    @Override
    public void OnWebServiceError(String errorMessage) {
        Util.showAlertDialog(mContext, getString(R.string.servers_list),
                getString(R.string.service_error_msg),
                getString(R.string.done), "", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }, null, true);

    }

    @Override
    public void onServerClick(View view, String serverName) {
        //display server name
        Toast.makeText(mContext, getString(R.string.server_name) + " " + serverName, Toast.LENGTH_SHORT).show();
    }

    private ServersListAdapter.OnLoadMoreListener loadMoreListener =
            new ServersListAdapter.OnLoadMoreListener() {
                @Override
                public void onLoadMore() {

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mServersAdapter.addOtherServer(null);
                            getServersDataByPage(false);
                        }
                    }, Constants.LOAD_MORE_DELAY);

                }
            };
}

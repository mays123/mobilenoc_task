package com.cloudconnectiv.mobilenoc.features.shared;

import android.os.Parcelable;

/**
 * Created by MaysAtari on 1/26/2018.
 */

public abstract class BasePojo implements Parcelable {
    @Override
    public String toString() {
        return this.getClass().getSimpleName();
    }
}
package com.cloudconnectiv.mobilenoc.features.shared;

import android.content.Context;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.ImageViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cloudconnectiv.mobilenoc.R;
import com.cloudconnectiv.mobilenoc.utility.ContextLangWrapper;
import com.cloudconnectiv.mobilenoc.utility.Util;


/**
 * Created by mays on 1/20/2017.
 */
public abstract class BaseActivity extends AppCompatActivity implements View.OnClickListener {

    public Toolbar mToolbar;
    private Context ctx = this;
    private RelativeLayout mAllLocationsLayout;
    private TextView mAllLocationsTxt;
    private ImageView mAllLocationsIcon;
    private boolean isAllLocationsSelected = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        try {
            setContentView(getActivityLayout());

            initVariables();
            initToolBar();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    private void initToolBar() {
        if (mToolbar != null && getSupportActionBar() != null) {
            setSupportActionBar(mToolbar);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
    }


    private void initVariables() {
        mToolbar = findViewById(R.id.toolbar);
        mAllLocationsLayout = findViewById(R.id.rlAllLocations);
        mAllLocationsTxt = findViewById(R.id.tvAllLocations);
        mAllLocationsIcon = findViewById(R.id.ivAllLocations);

        mAllLocationsLayout.setOnClickListener(this);

    }


    protected abstract int getActivityLayout();


    @Override
    protected void attachBaseContext(Context newBase) {

        // .. to make the app in english
        Context context = ContextLangWrapper.wrap(newBase, "en");
        super.attachBaseContext(context);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.rlAllLocations:
                changeAllLocationsState();
                break;
        }
    }

    private void changeAllLocationsState() {
        if (isAllLocationsSelected) {
            //all locations button is off
            Util.changeViewBackgroundDrawable(ctx, mAllLocationsLayout,
                    R.drawable.not_selected_location_background);
            mAllLocationsTxt.setTextColor(ContextCompat.getColor(ctx, R.color.lightGrey));
            ImageViewCompat.setImageTintList(mAllLocationsIcon, ColorStateList.valueOf(ContextCompat.getColor
                    (ctx, R.color.darkGrey)));

        } else {
            //all locations button is on
            Util.changeViewBackgroundDrawable(ctx, mAllLocationsLayout,
                    R.drawable.selected_location_background);
            mAllLocationsTxt.setTextColor(ContextCompat.getColor(ctx, R.color.white));
            ImageViewCompat.setImageTintList(mAllLocationsIcon, ColorStateList.valueOf(ContextCompat.getColor
                    (ctx, R.color.white)));
        }
        isAllLocationsSelected = !isAllLocationsSelected;
    }
}

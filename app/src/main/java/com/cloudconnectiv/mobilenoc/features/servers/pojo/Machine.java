package com.cloudconnectiv.mobilenoc.features.servers.pojo;

import android.os.Parcel;

import com.cloudconnectiv.mobilenoc.features.shared.BasePojo;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by MaysAtari on 1/26/2018.
 */

public class Machine extends BasePojo {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("ipAddress")
    @Expose
    private String ipAddress;
    @SerializedName("ipSubnetMask")
    @Expose
    private String ipSubnetMask;
    @SerializedName("model")
    @Expose
    private Model model;
    @SerializedName("status")
    @Expose
    private Status status;
    @SerializedName("type")
    @Expose
    private Type type;
    @SerializedName("serialNum")
    @Expose
    private String serialNum;
    @SerializedName("communicationProtocols")
    @Expose
    private List<CommunicationProtocol> communicationProtocols = new ArrayList<>();
    @SerializedName("targetMachines")
    @Expose
    private List<TargetMachines> targetMachines = new ArrayList<>();
    @SerializedName("location")
    @Expose
    private Integer location;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getIpSubnetMask() {
        return ipSubnetMask;
    }

    public void setIpSubnetMask(String ipSubnetMask) {
        this.ipSubnetMask = ipSubnetMask;
    }

    public Model getModel() {
        return model;
    }

    public void setModel(Model model) {
        this.model = model;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public String getSerialNum() {
        return serialNum;
    }

    public void setSerialNum(String serialNum) {
        this.serialNum = serialNum;
    }

    public List<CommunicationProtocol> getCommunicationProtocols() {
        return communicationProtocols;
    }

    public void setCommunicationProtocols(List<CommunicationProtocol> communicationProtocols) {
        this.communicationProtocols = communicationProtocols;
    }

    public List<TargetMachines> getTargetMachines() {
        return targetMachines;
    }

    public void setTargetMachines(List<TargetMachines> targetMachines) {
        this.targetMachines = targetMachines;
    }

    public Integer getLocation() {
        return location;
    }

    public void setLocation(Integer location) {
        this.location = location;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.id);
        dest.writeString(this.name);
        dest.writeString(this.ipAddress);
        dest.writeString(this.ipSubnetMask);
        dest.writeParcelable(this.model, flags);
        dest.writeParcelable(this.status, flags);
        dest.writeParcelable(this.type, flags);
        dest.writeString(this.serialNum);
        dest.writeTypedList(this.communicationProtocols);
        dest.writeTypedList(this.targetMachines);
        dest.writeValue(this.location);
    }

    public Machine() {
    }

    protected Machine(Parcel in) {
        this.id = (Integer) in.readValue(Integer.class.getClassLoader());
        this.name = in.readString();
        this.ipAddress = in.readString();
        this.ipSubnetMask = in.readString();
        this.model = in.readParcelable(Model.class.getClassLoader());
        this.status = in.readParcelable(Status.class.getClassLoader());
        this.type = in.readParcelable(Type.class.getClassLoader());
        this.serialNum = in.readString();
        this.communicationProtocols = in.createTypedArrayList(CommunicationProtocol.CREATOR);
        this.targetMachines = in.createTypedArrayList(TargetMachines.CREATOR);
        this.location = (Integer) in.readValue(Integer.class.getClassLoader());
    }

    public static final Creator<Machine> CREATOR = new Creator<Machine>() {
        @Override
        public Machine createFromParcel(Parcel source) {
            return new Machine(source);
        }

        @Override
        public Machine[] newArray(int size) {
            return new Machine[size];
        }
    };
}

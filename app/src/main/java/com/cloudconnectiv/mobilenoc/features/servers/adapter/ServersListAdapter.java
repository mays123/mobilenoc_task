package com.cloudconnectiv.mobilenoc.features.servers.adapter;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.cloudconnectiv.mobilenoc.R;
import com.cloudconnectiv.mobilenoc.features.servers.pojo.Content;
import com.cloudconnectiv.mobilenoc.utility.Constants;
import com.cloudconnectiv.mobilenoc.utility.Util;

import java.util.List;
import java.util.Locale;

/**
 * Created by MaysAtari on 1/26/2018.
 */

public class ServersListAdapter extends RecyclerView.Adapter {

    private List<Content> mDownServersList;
    private List<Content> mOtherServersList;
    private Context mContext;
    private LayoutInflater mInflater;
    private OnServerItemClickListener mOnServerItemClick;
    private int lastVisibleItem, totalItemCount;
    private OnLoadMoreListener onLoadMoreListener;
    private boolean loading;
    private final int VIEW_PROG = 0;
    private final int VIEW_TYPE_DOWN_SERVERS = 1;
    private final int VIEW_TYPE_SEPARATOR = 2;
    private final int VIEW_TYPE_OTHER_SERVERS = 3;
    private boolean isSeparatorAdded = false;


    public interface OnServerItemClickListener {
        void onServerClick(View view, String serverName);
    }

    public interface OnLoadMoreListener {
        void onLoadMore();
    }

    public void addServers(List<Content> mDownList, List<Content> mOtherList) {
        mDownServersList.addAll(mDownList);
        Log.d("downList size", String.valueOf(mDownList.size()));


        if (!isSeparatorAdded) {//all servers separator
            mOtherServersList.add(new Content());//reserve position 0 for the separator
            isSeparatorAdded = true;
        }

        mOtherServersList.addAll(mOtherList);
        Log.d("otherList size", String.valueOf(mOtherList.size()));

        notifyDataSetChanged();
    }

    public void addRangeServers(List<Content> mDownList, List<Content> mOtherList, int startPosition,
                                int count) {
        mDownServersList.addAll(mDownList);
        Log.d("downList size", String.valueOf(mDownList.size()));

        mOtherServersList.addAll(mOtherList);
        Log.d("otherList size", String.valueOf(mOtherList.size()));

        notifyItemRangeInserted(startPosition, count);
    }

    public void addOtherServer(Content otherServer) {
        mOtherServersList.add(otherServer);
        Log.d("otherList size", String.valueOf(mOtherServersList.size()));

        notifyItemInserted((mDownServersList.size() + mOtherServersList.size()) - 1);
    }

    public void removeOtherServer(int position) {
        mOtherServersList.remove(position);
        notifyItemRemoved(mDownServersList.size() + mOtherServersList.size());

        Log.d("otherList size", String.valueOf(mOtherServersList.size()));
    }

    public ServersListAdapter(Context mContext, List<Content> mDownServersList,
                              List<Content> mOtherServersList, OnServerItemClickListener mOnServerItemClick,
                              RecyclerView recyclerView) {
        this.mDownServersList = mDownServersList;
        this.mOtherServersList = mOtherServersList;
        this.mContext = mContext;
        this.mInflater = LayoutInflater.from(mContext);
        this.mOnServerItemClick = mOnServerItemClick;

        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {
            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView
                    .getLayoutManager();

            recyclerView
                    .addOnScrollListener(new RecyclerView.OnScrollListener() {
                        @Override
                        public void onScrolled(RecyclerView recyclerView,
                                               int dx, int dy) {
                            super.onScrolled(recyclerView, dx, dy);

                            totalItemCount = linearLayoutManager.getItemCount();
                            lastVisibleItem = linearLayoutManager
                                    .findLastVisibleItemPosition();
                            if (!loading
                                    && lastVisibleItem >= Constants.PAGE_SIZE - 1
                                    && totalItemCount <= (lastVisibleItem + Constants.PAGE_SIZE
                            )) {
                                // End has been reached
                                // Do something
                                if (onLoadMoreListener != null) {
                                    onLoadMoreListener.onLoadMore();
                                }
                                loading = true;
                            }
                        }
                    });
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view;
        switch (viewType) {
            case VIEW_TYPE_DOWN_SERVERS:
                view = mInflater.inflate(R.layout.server_list_item, parent, false);
                return new DownServersViewHolder(view);

            case VIEW_TYPE_SEPARATOR:
                view = mInflater.inflate(R.layout.servers_separator, parent, false);
                return new SeparatorViewHolder(view);

            case VIEW_TYPE_OTHER_SERVERS:
                //TODO change item
                view = mInflater.inflate(R.layout.server_list_item, parent, false);
                return new OtherServersViewHolder(view);

        }

        view = mInflater.inflate(R.layout.servers_progress_list_item, parent, false);

        return new ProgressViewHolder(view);

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof DownServersViewHolder) {
            Content contentPojo = mDownServersList.get(position);
            ((DownServersViewHolder) holder).populateData(contentPojo);

        } else if (holder instanceof SeparatorViewHolder) {
            //show the separator

        } else if (holder instanceof OtherServersViewHolder) {
            Content contentPojo = mOtherServersList.get(position - mDownServersList.size());
            ((OtherServersViewHolder) holder).populateData(contentPojo);

        } else if (holder instanceof ProgressViewHolder) {
            ((ProgressViewHolder) holder).progressBar.setIndeterminate(true);
        }


    }

    private void populateData(RecyclerView.ViewHolder holder, Content contentPojo) {


    }


    private void changeActionButtonStatus(boolean isBtnSelected, ImageView mActionBtn, int drawable) {
        if (isBtnSelected) {
            Util.changeViewBackgroundDrawable(mContext, mActionBtn, R.drawable.grey_background_circle);
        } else {
            Util.changeViewBackgroundDrawable(mContext, mActionBtn, drawable);
        }
    }

    private void handleActionButtonStatus(boolean isBtnSelected, ImageView mActionBtn, int drawable) {
        if (isBtnSelected) {
            Util.changeViewBackgroundDrawable(mContext, mActionBtn, drawable);
        } else {
            Util.changeViewBackgroundDrawable(mContext, mActionBtn, R.drawable.grey_background_circle);
        }
    }

    private void handleServerStatus(View mStatusView, Integer statusId) {
        if (statusId == 1) {//CYAN
            Util.changeViewBackgroundDrawable(mContext, mStatusView, R.drawable.status_cyan_round_circle);
        } else if (statusId == 2) {//ORANGE
            Util.changeViewBackgroundDrawable(mContext, mStatusView, R.drawable.status_orange_round_circle);
        } else if (statusId == 3) {//YELLOW
            Util.changeViewBackgroundDrawable(mContext, mStatusView, R.drawable.status_yellow_round_circle);
        } else if (statusId == 4) {//RED
            Util.changeViewBackgroundDrawable(mContext, mStatusView, R.drawable.status_red_round_circle);
        }
    }

    private void handleReasonText(TextView mDownReasonTxt, String statusValue) {
        //to show reason TextView only if the server is down
        if (statusValue.equalsIgnoreCase("INSTANCE_STATE_DOWN")) {
            mDownReasonTxt.setVisibility(View.VISIBLE);
        } else {
            mDownReasonTxt.setVisibility(View.INVISIBLE);
        }
    }


    @Override
    public int getItemViewType(int position) {

        //show progress
        if (position == ((mDownServersList.size() + mOtherServersList.size()) - 1)
                && mOtherServersList.get(mOtherServersList.size() - 1) == null) {
            return VIEW_PROG;
        }

        //down servers list
        if (position < mDownServersList.size()) {
            return VIEW_TYPE_DOWN_SERVERS;
        }

        //all servers separator
        if (position - mDownServersList.size() == 0) {
            return VIEW_TYPE_SEPARATOR;
        }

        //other servers list
        if ((position - mDownServersList.size() < mOtherServersList.size())) {
            return VIEW_TYPE_OTHER_SERVERS;
        }

        return -1;

    }

    public void setLoaded() {
        loading = false;
    }

    public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }


    @Override
    public int getItemCount() {
        return mDownServersList.size() + mOtherServersList.size();
    }

    public class DownServersViewHolder extends RecyclerView.ViewHolder {

        ImageView mDoneImg, mCallImg, mTimerImg, mSoundImg;
        TextView mDownReasonTxt, mCountryNameTxt, mServerNameTxt, mServerIpAddressTxt,
                mServerIpSubnetTxt;
        View mStatusView;
        LinearLayout mServerItemLayout;

        DownServersViewHolder(View itemView) {
            super(itemView);
            mDoneImg = itemView.findViewById(R.id.ivDone);
            mCallImg = itemView.findViewById(R.id.ivCall);
            mTimerImg = itemView.findViewById(R.id.ivTimer);
            mSoundImg = itemView.findViewById(R.id.ivSound);
            mDownReasonTxt = itemView.findViewById(R.id.tvDownReason);
            mCountryNameTxt = itemView.findViewById(R.id.tvServerCountry);
            mServerNameTxt = itemView.findViewById(R.id.tvServerName);
            mServerIpAddressTxt = itemView.findViewById(R.id.tvServerIpAddress);
            mServerIpSubnetTxt = itemView.findViewById(R.id.tvServerIpSubnet);
            mStatusView = itemView.findViewById(R.id.vStatus);
            mServerItemLayout = itemView.findViewById(R.id.llServerItem);
        }

        void populateData(Content contentPojo) {
            mCountryNameTxt.setText(String.format(Locale.ENGLISH,
                    "%s%d", mContext.getString(R.string.location_id),
                    contentPojo.getLocation()));
            mServerNameTxt.setText(contentPojo.getName());
            mServerIpAddressTxt.setText(contentPojo.getIpAddress());
            mServerIpSubnetTxt.setText(contentPojo.getIpSubnetMask());

            handleReasonText(mDownReasonTxt, contentPojo.getStatus().getStatusValue());
            handleServerStatus(mStatusView, contentPojo.getStatus().getId());


            //to handle unexpected behavior on data recycling
            handleActionButtonStatus(contentPojo.isDone(), mDoneImg, R.drawable.purple_background_circle);
            handleActionButtonStatus(contentPojo.isCall(), mCallImg, R.drawable.mauve_background_circle);
            handleActionButtonStatus(contentPojo.isTimer(), mTimerImg, R.drawable.turquoise_background_circle);
            handleActionButtonStatus(contentPojo.isMute(), mSoundImg, R.drawable.red_background_circle);


            //////////listeners

            mServerItemLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //server item is clicked
                    mOnServerItemClick.onServerClick(view, contentPojo.getName());
                }
            });


            mDoneImg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //change color depending on button status
                    changeActionButtonStatus(contentPojo.isDone(), mDoneImg,
                            R.drawable.purple_background_circle);
                    contentPojo.setDone(!contentPojo.isDone());
                }
            });

            mCallImg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //change color depending on button status
                    changeActionButtonStatus(contentPojo.isCall(), mCallImg,
                            R.drawable.mauve_background_circle);
                    contentPojo.setCall(!contentPojo.isCall());
                }
            });

            mTimerImg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //change color depending on button status
                    changeActionButtonStatus(contentPojo.isTimer(), mTimerImg,
                            R.drawable.turquoise_background_circle);
                    contentPojo.setTimer(!contentPojo.isTimer());

                }
            });

            mSoundImg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //change color depending on button status
                    changeActionButtonStatus(contentPojo.isMute(), mSoundImg,
                            R.drawable.red_background_circle);
                    contentPojo.setMute(!contentPojo.isMute());
                }
            });
        }
    }

    public class OtherServersViewHolder extends RecyclerView.ViewHolder {

        ImageView mDoneImg, mCallImg, mTimerImg, mSoundImg;
        TextView mDownReasonTxt, mCountryNameTxt, mServerNameTxt, mServerIpAddressTxt,
                mServerIpSubnetTxt;
        View mStatusView;
        LinearLayout mServerItemLayout;

        OtherServersViewHolder(View itemView) {
            super(itemView);
            mDoneImg = itemView.findViewById(R.id.ivDone);
            mCallImg = itemView.findViewById(R.id.ivCall);
            mTimerImg = itemView.findViewById(R.id.ivTimer);
            mSoundImg = itemView.findViewById(R.id.ivSound);
            mDownReasonTxt = itemView.findViewById(R.id.tvDownReason);
            mCountryNameTxt = itemView.findViewById(R.id.tvServerCountry);
            mServerNameTxt = itemView.findViewById(R.id.tvServerName);
            mServerIpAddressTxt = itemView.findViewById(R.id.tvServerIpAddress);
            mServerIpSubnetTxt = itemView.findViewById(R.id.tvServerIpSubnet);
            mStatusView = itemView.findViewById(R.id.vStatus);
            mServerItemLayout = itemView.findViewById(R.id.llServerItem);
        }

        void populateData(Content contentPojo) {
            mCountryNameTxt.setText(String.format(Locale.ENGLISH,
                    "%s%d", mContext.getString(R.string.location_id),
                    contentPojo.getLocation()));
            mServerNameTxt.setText(contentPojo.getName());
            mServerIpAddressTxt.setText(contentPojo.getIpAddress());
            mServerIpSubnetTxt.setText(contentPojo.getIpSubnetMask());

            handleServerStatus(mStatusView, contentPojo.getStatus().getId());

            //to handle unexpected behavior on data recycling
            handleActionButtonStatus(contentPojo.isDone(), mDoneImg, R.drawable.purple_background_circle);
            handleActionButtonStatus(contentPojo.isCall(), mCallImg, R.drawable.mauve_background_circle);
            handleActionButtonStatus(contentPojo.isTimer(), mTimerImg, R.drawable.turquoise_background_circle);
            handleActionButtonStatus(contentPojo.isMute(), mSoundImg, R.drawable.red_background_circle);


            //////////listeners

            mServerItemLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //server item is clicked
                    mOnServerItemClick.onServerClick(view, contentPojo.getName());
                }
            });


            mDoneImg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //change color depending on button status
                    changeActionButtonStatus(contentPojo.isDone(), mDoneImg,
                            R.drawable.purple_background_circle);
                    contentPojo.setDone(!contentPojo.isDone());
                }
            });

            mCallImg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //change color depending on button status
                    changeActionButtonStatus(contentPojo.isCall(), mCallImg,
                            R.drawable.mauve_background_circle);
                    contentPojo.setCall(!contentPojo.isCall());
                }
            });

            mTimerImg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //change color depending on button status
                    changeActionButtonStatus(contentPojo.isTimer(), mTimerImg,
                            R.drawable.turquoise_background_circle);
                    contentPojo.setTimer(!contentPojo.isTimer());

                }
            });

            mSoundImg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //change color depending on button status
                    changeActionButtonStatus(contentPojo.isMute(), mSoundImg,
                            R.drawable.red_background_circle);
                    contentPojo.setMute(!contentPojo.isMute());
                }
            });
        }


    }

    public class SeparatorViewHolder extends RecyclerView.ViewHolder {

        SeparatorViewHolder(View itemView) {
            super(itemView);
        }

    }

    //progress holder
    private static class ProgressViewHolder extends RecyclerView.ViewHolder {
        ProgressBar progressBar;

        ProgressViewHolder(View v) {
            super(v);
            progressBar = (ProgressBar) v.findViewById(R.id.progress_bar);
        }
    }
}

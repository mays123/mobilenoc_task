package com.cloudconnectiv.mobilenoc.features.servers.pojo;

import android.os.Parcel;

import com.cloudconnectiv.mobilenoc.features.shared.BasePojo;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by MaysAtari on 1/26/2018.
 */

public class TargetMachines extends BasePojo {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("sourceMachineId")
    @Expose
    private Integer sourceMachineId;
    @SerializedName("targetMachine")
    @Expose
    private Machine targetMachine;
    @SerializedName("circuitStatusId")
    @Expose
    private Integer circuitStatusId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getSourceMachineId() {
        return sourceMachineId;
    }

    public void setSourceMachineId(Integer sourceMachineId) {
        this.sourceMachineId = sourceMachineId;
    }

    public Machine getTargetMachine() {
        return targetMachine;
    }

    public void setTargetMachine(Machine targetMachine) {
        this.targetMachine = targetMachine;
    }

    public Integer getCircuitStatusId() {
        return circuitStatusId;
    }

    public void setCircuitStatusId(Integer circuitStatusId) {
        this.circuitStatusId = circuitStatusId;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.id);
        dest.writeValue(this.sourceMachineId);
        dest.writeParcelable(this.targetMachine, flags);
        dest.writeValue(this.circuitStatusId);
    }

    public TargetMachines() {
    }

    protected TargetMachines(Parcel in) {
        this.id = (Integer) in.readValue(Integer.class.getClassLoader());
        this.sourceMachineId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.targetMachine = in.readParcelable(Machine.class.getClassLoader());
        this.circuitStatusId = (Integer) in.readValue(Integer.class.getClassLoader());
    }

    public static final Creator<TargetMachines> CREATOR = new Creator<TargetMachines>() {
        @Override
        public TargetMachines createFromParcel(Parcel source) {
            return new TargetMachines(source);
        }

        @Override
        public TargetMachines[] newArray(int size) {
            return new TargetMachines[size];
        }
    };
}

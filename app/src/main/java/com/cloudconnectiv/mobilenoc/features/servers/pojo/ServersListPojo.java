package com.cloudconnectiv.mobilenoc.features.servers.pojo;

import android.os.Parcel;

import com.cloudconnectiv.mobilenoc.features.shared.BasePojo;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by MaysAtari on 1/26/2018.
 */

public class ServersListPojo extends BasePojo {

    @SerializedName("content")
    @Expose
    private List<Content> content = new ArrayList<>();
    @SerializedName("totalPages")
    @Expose
    private Integer totalPages;
    @SerializedName("totalElements")
    @Expose
    private Integer totalElements;
    @SerializedName("last")
    @Expose
    private Boolean last;
    @SerializedName("first")
    @Expose
    private Boolean first;
    @SerializedName("sort")
    @Expose
    private String sort;
    @SerializedName("numberOfElements")
    @Expose
    private Integer numberOfElements;
    @SerializedName("size")
    @Expose
    private Integer size;
    @SerializedName("number")
    @Expose
    private Integer number;

    public List<Content> getContent() {
        return content;
    }

    public void setContent(List<Content> content) {
        this.content = content;
    }

    public Integer getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(Integer totalPages) {
        this.totalPages = totalPages;
    }

    public Integer getTotalElements() {
        return totalElements;
    }

    public void setTotalElements(Integer totalElements) {
        this.totalElements = totalElements;
    }

    public Boolean getLast() {
        return last;
    }

    public void setLast(Boolean last) {
        this.last = last;
    }

    public Boolean getFirst() {
        return first;
    }

    public void setFirst(Boolean first) {
        this.first = first;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public Integer getNumberOfElements() {
        return numberOfElements;
    }

    public void setNumberOfElements(Integer numberOfElements) {
        this.numberOfElements = numberOfElements;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(this.content);
        dest.writeValue(this.totalPages);
        dest.writeValue(this.totalElements);
        dest.writeValue(this.last);
        dest.writeValue(this.first);
        dest.writeString(this.sort);
        dest.writeValue(this.numberOfElements);
        dest.writeValue(this.size);
        dest.writeValue(this.number);
    }

    public ServersListPojo() {
    }

    protected ServersListPojo(Parcel in) {
        this.content = in.createTypedArrayList(Content.CREATOR);
        this.totalPages = (Integer) in.readValue(Integer.class.getClassLoader());
        this.totalElements = (Integer) in.readValue(Integer.class.getClassLoader());
        this.last = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.first = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.sort = in.readString();
        this.numberOfElements = (Integer) in.readValue(Integer.class.getClassLoader());
        this.size = (Integer) in.readValue(Integer.class.getClassLoader());
        this.number = (Integer) in.readValue(Integer.class.getClassLoader());
    }

    public static final Creator<ServersListPojo> CREATOR = new Creator<ServersListPojo>() {
        @Override
        public ServersListPojo createFromParcel(Parcel source) {
            return new ServersListPojo(source);
        }

        @Override
        public ServersListPojo[] newArray(int size) {
            return new ServersListPojo[size];
        }
    };
}

package com.cloudconnectiv.mobilenoc.features.servers.pojo;

import android.os.Parcel;

import com.cloudconnectiv.mobilenoc.features.shared.BasePojo;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by MaysAtari on 1/26/2018.
 */

public class CommunicationProtocol extends BasePojo {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("defaultPort")
    @Expose
    private Integer defaultPort;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getDefaultPort() {
        return defaultPort;
    }

    public void setDefaultPort(Integer defaultPort) {
        this.defaultPort = defaultPort;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.id);
        dest.writeString(this.name);
        dest.writeValue(this.defaultPort);
    }

    public CommunicationProtocol() {
    }

    protected CommunicationProtocol(Parcel in) {
        this.id = (Integer) in.readValue(Integer.class.getClassLoader());
        this.name = in.readString();
        this.defaultPort = (Integer) in.readValue(Integer.class.getClassLoader());
    }

    public static final Creator<CommunicationProtocol> CREATOR = new Creator<CommunicationProtocol>() {
        @Override
        public CommunicationProtocol createFromParcel(Parcel source) {
            return new CommunicationProtocol(source);
        }

        @Override
        public CommunicationProtocol[] newArray(int size) {
            return new CommunicationProtocol[size];
        }
    };
}

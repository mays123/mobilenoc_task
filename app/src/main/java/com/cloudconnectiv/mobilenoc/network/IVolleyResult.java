package com.cloudconnectiv.mobilenoc.network;

import com.android.volley.VolleyError;

/**
 * Created by mays on 11/29/2016.
 */
public interface IVolleyResult {
    public void notifySuccess(String response);
    public void notifyError(VolleyError error);
}

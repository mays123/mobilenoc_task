package com.cloudconnectiv.mobilenoc.network;

/**
 * Created by MaysAtari on 8/31/2017.
 */

public interface IServiceObjectCallBack {
    <T> void onWebServiceSuccess(T returnedObject);
    void OnWebServiceError(String errorMessage);
}

package com.cloudconnectiv.mobilenoc.network;

import android.content.Context;
import android.content.DialogInterface;
import android.util.Base64;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.cloudconnectiv.mobilenoc.R;
import com.cloudconnectiv.mobilenoc.app.AppController;
import com.cloudconnectiv.mobilenoc.utility.CustomProgressDialog;
import com.cloudconnectiv.mobilenoc.utility.Util;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by mays on 11/28/2016.
 */
public class VolleyManager {

    private IVolleyResult mResultCallback = null;
    private Context mContext;
    private CustomProgressDialog customProgressDialog;


    public VolleyManager(IVolleyResult resultCallback, Context context) {
        mResultCallback = resultCallback;
        mContext = context;

        customProgressDialog = new CustomProgressDialog(context);
    }

    public void makeJSONGetObjectRequest(Context context, String url, int JSONTag,
                                         final boolean showProgress) {

        Log.i("url ", url);

        if (!Util.isNetworkAvailable(context)) {
            Util.showAlertDialog(context, "", context.getString(R.string.no_internet),
                    context.getString(R.string.done), null, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    }, null, false);
            return;
        }

        if (showProgress)
            customProgressDialog.showCustomDialog();

        JsonObjectRequest req = new JsonObjectRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        VolleyLog.d("JSON success", response.toString());
                        if (mResultCallback != null)
                            mResultCallback.notifySuccess(response.toString());

                        if (showProgress)
                            customProgressDialog.dismissCustomDialog();
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyLog.e("JSON error", error.getMessage());
                        if (mResultCallback != null)
                            mResultCallback.notifyError(error);

                        if (showProgress)
                            customProgressDialog.dismissCustomDialog();
                    }
                }) {

            /**
             * Passing some request headers
             */
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                String creds = String.format("%s:%s", "admin@boot.com", "admin");
                String auth = "Basic " + Base64.encodeToString(creds.getBytes(), Base64.DEFAULT);
                headers.put("Authorization", auth);
                return headers;
            }


        };
        req.setShouldCache(false);
        req.setRetryPolicy(new DefaultRetryPolicy(
                999999999,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(req, String.valueOf(JSONTag));
    }


}

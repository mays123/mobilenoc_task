package com.cloudconnectiv.mobilenoc.utility;

/**
 * Created by mays on 11/28/2016.
 */
public class Constants {

    public static final String BASE = "https://45.55.43.15:9090/";

    public static final String BASE_SUB = "api/";

    public static final String SERVERS_LIST_LINK = BASE + BASE_SUB + "machine?page=%1$s&size=%2$s";

    //load more list
    public static final long LOAD_MORE_DELAY = 0;
    public static final int PAGE_SIZE = 10;


    public static String getServersListLink() {
        return SERVERS_LIST_LINK;
    }
}

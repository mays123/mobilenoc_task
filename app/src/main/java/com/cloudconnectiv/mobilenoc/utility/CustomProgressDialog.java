package com.cloudconnectiv.mobilenoc.utility;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.Window;

import com.cloudconnectiv.mobilenoc.R;


/**
 * Created by mays on 1/8/2017.
 */
public class CustomProgressDialog {

    private Dialog dialog;
    private Context context;

    public CustomProgressDialog(Context context) {
        dialog = new Dialog(context);
        this.context = context;
    }


    public void showCustomDialog() {
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_custom_progress);
        dialog.setCancelable(false);

        Window window = dialog.getWindow();
        if (window != null)
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        dialog.show();
    }


    public void hideCustomDialog() {
        if (dialog.isShowing())
            dialog.hide();
    }

    public void dismissCustomDialog() {
        if (dialog.isShowing())
            dialog.dismiss();
    }

}

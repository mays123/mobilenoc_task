package com.cloudconnectiv.mobilenoc.utility;

import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AlertDialog;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;

/**
 * Created by MaysAtari on 1/26/2018.
 */

public class Util {

    /**
     * check for internet connectivity
     */
    public static boolean isNetworkAvailable(Context c) {
        ConnectivityManager cm = (ConnectivityManager) c.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo networkInfo = null;
        if (cm != null) networkInfo = cm.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isConnectedOrConnecting()) {
            NetworkInfo wifi = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            NetworkInfo mobile = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            if ((mobile != null && mobile.isConnectedOrConnecting()) || (wifi != null && wifi.isConnectedOrConnecting()))
                return true;
            else return false;
        } else
            return false;
    }

    /**
     *convert dp unit to pixels
     */
    public static int convertDpToPx(int dp) {
        DisplayMetrics displayMetrics = Resources.getSystem().getDisplayMetrics();
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, displayMetrics);
    }


    public static void showAlertDialog(final Context ctx, String title, String msg,
                                       String positiveBtn, String negativeBtn,
                                       DialogInterface.OnClickListener pos,
                                       DialogInterface.OnClickListener neg,
                                       boolean isCancelable) {

        try {
            AlertDialog alertdialog = new AlertDialog.Builder(ctx)
                    .setTitle(title)
                    .setMessage(msg)
                    .setPositiveButton(positiveBtn, pos)
                    .setNegativeButton(negativeBtn, neg)
                    .setCancelable(isCancelable)
                    .create();

            alertdialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     *change view background drawable
     */
    public static void changeViewBackgroundDrawable(Context context, View view, int drawable) {
        view.setBackground(ResourcesCompat.getDrawable(context.getResources(), drawable, null));
    }

}

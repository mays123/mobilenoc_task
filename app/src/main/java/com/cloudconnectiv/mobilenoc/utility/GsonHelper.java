package com.cloudconnectiv.mobilenoc.utility;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by MaysAtari on 8/29/2017.
 */

public class GsonHelper {


    public static <T> ArrayList<T> getParsedArrayList(String jsonString, Class<T[]> clazz) {
        T[] arr = new Gson().fromJson(jsonString, clazz);
        return new ArrayList<>(Arrays.asList(arr));
    }

    public static <T> T getParsedObject(String jsonString, Class<T> clazz) {
        T object = new Gson().fromJson(jsonString, clazz);
        return object;
    }
}
